#Install example

Open console with Admin.

    // install modules for node.js
    npm install
    // run gulp
    gulp build
    // check dist folder
    // server
    gulp server
    // enter to admin
    user : demo@gmail.com
    pass: demo

**That's it!**

Example: https://dtbase.firebaseapp.com/


### create your own application

- Change url in data.js firebaseUrl
- Create account and database on firebase
- Set basic rules (check example after)
- Import demo data on database
- Create users if you like or register on the app


**Firebase rules:**

    {
      "rules": {
        "users": {
          "$userid": {
            ".read": "auth.uid == $userid",
            ".write": "auth.uid == $userid",
            // users can login but not write or edit data
            //".write": "data.exists() && auth.uid === $userid && newData.exists()"
            ".validate": "newData.hasChildren(['public'])"
          }
        }
      }
    }

