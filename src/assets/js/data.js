/** = rules for secutiry on firebase
======================================= */
//{
//  "rules": {
//    "users": {
//      "$userid": {
//        ".read": "auth.uid == $userid",
//        ".write": "auth.uid == $userid",
//        // users can login but not write or edit data
//        //".write": "data.exists() && auth.uid === $userid && newData.exists()"
//        ".validate": "newData.hasChildren(['public'])"
//      }
//    }
//  }
//}


/** =  Firebase url
============================== */
var firebaseUrl = 'https://dtbase.firebaseio.com',
    /** =  Init data var
    ============================== */
    data = [],
    json = [],
    editor = '',
    /** =  Storage
    ============================== */
    storage = window.sessionStorage,
    /** =  init Firebase
    ============================== */
    fbase = new Firebase(firebaseUrl + "/users/" + storage.getItem('user_uid')),
    /** =  Snippet var
    ============================== */
    data_snippet = '',
    /** =  Notifications
    ============================== */
    err_login = {
        title: 'Error',
        msg: 'you must need login to read data'
    },
    err_sync = {
        title: 'Error !',
        msg: 'Synchronization failed'
    },
    done_sync = {
        title: 'Success',
        msg: 'Synchronization done'
    },
    err_save = {
        title: 'Error!',
        msg: 'Empty fields please write !'
    },
    done_save = {
        title: 'Success',
        msg: 'The data has been Save!'
    },
    done_remove = {
        title: 'Success',
        msg: 'The data has been Deleted!'
    },
    done_update = {
        title: 'Success',
        msg: 'The data has been Updated!'
    },
    /** =  login data
    ============================== */
    data_login = {
        title: "Login",
        register:{
            title:'Sign in'
        },
        reset:{
            title:'Reset password'
        },
        label_email: 'Email',
        label_password: 'Password',
        label_password_confirm: 'Repeat Password',
        submit: 'Enter'
    },
    /** =  Dashboard data
    ============================== */
    data_dashboard = {
        title: "Dashboard",
        logo: {
            name: (storage.getItem('user_email')) ? storage.getItem('user_email').match(/^([^@]*)@/)[1] : '',
            thumb: storage.getItem('user_thumb') || 'assets/img/default.png',
        },
        search: 'Search by title',
        nav: {
            create: 'New Snippet',
            logout: 'Logout'
        },
        editor: {
            title: 'Create new snippet',
            label_title: 'Title',
            label_description: 'Description',
            label_code: 'Code',
            label_codetype: 'Type of code'
        },
        snippets: {
            notFound: 'Loading data please wait....'
        },
        // for confirm
        sure: 'Are you sure!'
    };
