var app = (function() {
    'use strict';
    return {


        // ==========================================================
        //  --------------------- Init Events -----------------------
        // ==========================================================


        /**
         * Init events
         * @return function
         */
        init: function() {
            var self = this;
            // check login
            if (utils.isLogin()) {
                //render view template
                utils.render('#page', tmpl_dashboard, data_dashboard)
                    // Get data of firebase
                    .then(function(response) {
                        self.getData();
                    }, function(e) { console.log('Fail!', e); })
                    // init events
                    .then(function() {
                        // create function
                        self.create();
                        // logout
                        document.querySelector('.logout').addEventListener('click', function(e) {
                            e.preventDefault();
                            utils.logout();
                        });
                        // found errors
                    }, function(e) { console.log('Fail!', e); })
                    .catch(function(error) { console.log(error); });
            } else {
                //render view template
                utils.render('#page', tmpl_login, data_login)
                    // init login form events
                    // login
                    .then(function(response) {
                        self.form();
                    }, function(e) { console.log('Fail!', e); })
                    // register
                    .then(function(response) {
                        self.registerUser();
                    }, function(e) { console.log('Fail!', e); })
                    // reset password
                    .then(function(response) {
                        self.resetPass();
                    }, function(e) { console.log('Fail!', e); })
                    .catch(function(error) { console.log(error); });
            }
        },


        // ==========================================================
        //  --------------------- Register -------------------------
        // ==========================================================


        /**
         * Register user
         * @return function
         */
        registerUser: function() {
            var self = this,
                btn = document.querySelector('#register');
            // register btn
            btn.addEventListener('click', function() {
                //render view template
                return utils.render('#page', tmpl_register, data_login)
                    // init events for register
                    .then(function(response) {
                        self.back();
                        // register  submit
                        document.querySelector('#registerNewUser').addEventListener('click', function(e) {
                                e.preventDefault();
                                // check two passwords
                                var mail = document.querySelector('#email'),
                                    pass = document.querySelector('#pass'),
                                    pass2 = document.querySelector('#pass2');
                                // password not match
                                if (pass.value !== pass2.value) {
                                    // notification
                                    utils.notif('error', 'Error', "Password not match")
                                        .then(function() {
                                            pass.focus();
                                            pass.value = '';
                                            pass2.value = '';
                                        }, function(e) { console.log('Fail!', e); })
                                        .catch(function(error) { console.log(error); });
                                } else {
                                    // succes create user
                                    utils.ref.createUser({
                                        email: mail.value,
                                        password: pass.value
                                    }, function(error, userData) {
                                        // error show notifications
                                        if (error) {
                                            switch (error.code) {
                                                case "EMAIL_TAKEN":
                                                    utils.notif('error', 'EMAIL_TAKEN', 'The new user account cannot be created because the email is already in use.')
                                                        .then(function() {
                                                            pass.focus();
                                                            pass.value = '';
                                                            pass2.value = '';
                                                        }, function(e) { console.log('Fail!', e); });
                                                    break;
                                                case "INVALID_EMAIL":
                                                    utils.notif('error', 'INVALID_EMAIL', 'The specified email is not a valid email.')
                                                        .then(function() {
                                                            pass.focus();
                                                            pass.value = '';
                                                            pass2.value = '';
                                                        }, function(e) { console.log('Fail!', e); });
                                                    break;
                                                default:
                                                    utils.notif('error', 'Error', "Error creating user: " + error)
                                                        .then(function() {
                                                            pass.focus();
                                                            pass.value = '';
                                                            pass2.value = '';
                                                        }, function(e) { console.log('Fail!', e); });
                                            }
                                        } else {
                                            utils.notif('success', 'Success', "Successfully created user account with uid::" + userData.uid)
                                                .then(function() {
                                                    self.init();
                                                }, function(e) { console.log('Fail!', e); });
                                        }
                                        return false;
                                    });
                                }
                            }, function(e) { console.log('Fail!', e); });
                    });
            });
        },


        // ==========================================================
        //  --------------------- Reset -------------------------
        // ==========================================================


        /**
         * Reset password
         * @return function
         */
        resetPass: function() {
            var self = this;
            // btn reset
            document.querySelector('#resetPass').addEventListener('click', function(e) {
                e.preventDefault();
                // render reset template
                return utils.render('#page', tmpl_reset, data_login)
                    .then(function(response) {
                        // btn back
                        self.back();
                        // submit btn
                        document.querySelector('#resetPassword').addEventListener('click', function(e) {
                            e.preventDefault();
                            var mail = document.querySelector('#email');
                            utils.ref.resetPassword({
                                email: mail.value
                            }, function(error) {
                                if (error) {
                                    switch (error.code) {
                                        case "INVALID_USER":
                                            utils.notif('error', 'INVALID_USER', 'The specified user account does not exist.')
                                                .then(function() {
                                                    mail.focus();
                                                    mail.value = '';
                                                }, function(e) { console.log('Fail!', e); });
                                            break;
                                        default:
                                            utils.notif('error', 'Error', 'Error resetting password: ' + error)
                                                .then(function() {
                                                    mail.focus();
                                                    mail.value = '';
                                                }, function(e) { console.log('Fail!', e); });
                                            break;
                                    }
                                } else {
                                    utils.notif('success', 'Success', 'Password reset email sent successfully!')
                                        .then(function() {
                                           self.init();
                                        }, function(e) { console.log('Fail!', e); });
                                }
                                return false;
                            });
                        });
                    }, function(e) { console.log('Fail!', e); });
            });
        },


        // ==========================================================
        //  --------------------- Get Data -------------------------
        // ==========================================================


        /**
         * Get data of firebase
         * @return function
         */
        getData: function() {
            var self = this;
            // get data
            utils.getDb('public')
                // load template with data
                .then(function(d) {
                    // Compile Option html
                    var render = utils.tmpl(tmpl_snippet),
                        snp = document.querySelector('.snippets');
                    if (snp) snp.innerHTML = '';
                    for (var i in d) {
                        data = {
                            uid: i,
                            title: d[i].title,
                            type: d[i].type,
                            desc: utils.decode(d[i].desc),
                            code: utils.escapeHtml(utils.decode(d[i].code))
                        };
                        if (snp) snp.innerHTML += render(data);
                    }
                }, function(e) { console.log('Fail!', e); })
                // init events for template
                .then(function() {
                    // animate divs on start
                    utils.Animate('.shippet');
                    // filter links function
                    utils.FilterLinks('.filter', '.snippet');
                    // filter text
                    utils.FilterText('.filtertext', '.snippet-title');
                    // check all divs and init syntax
                    utils.Each('pre code', function(obj) {
                        return hljs.highlightBlock(obj);
                    });
                    // edit btn function
                    utils.Each('.edit', function(obj) {
                        obj.addEventListener('click', function(e) {
                            e.preventDefault();
                            return self.edit(this.getAttribute('data-uid'));
                        }, false);
                    });
                    // delete btn function
                    utils.Each('.delete', function(obj) {
                        obj.addEventListener('click', function(e) {
                            e.preventDefault();
                            var s = confirm(data_dashboard.sure);
                            if (s === true) return self.delete(this.getAttribute('data-uid'));
                            return false;
                        }, false);
                    });
                    // show errors
                }, function(e) { console.log('Fail!', e); });
        },


        // ==========================================================
        //  --------------------- Login  -------------------------
        // ==========================================================


        /**
         * Login form
         * @return function
         */
        form: function() {
            var self = this,
                login = document.querySelector('#login');
            if (login) {
                // click submit
                login.addEventListener('click', function(e) {
                    e.preventDefault();
                    var mail = document.querySelector('#email'),
                        pass = document.querySelector('#pass');
                    // login
                    utils.login({
                        email: mail.value,
                        password: pass.value
                    }, function(d) {
                        var header = (d.code) ? d.code : 'Success !',
                            msg = (d.message) ? d.message : 'Hello user';
                        utils.notif('', header, msg)
                            .then(function() {
                                if (utils.isLogin()) {
                                    window.location.href = window.location.href;
                                } else {
                                    mail.focus();
                                    mail.value = '';
                                    pass.value = '';
                                }
                                // errors
                            }, function(e) { console.log('Fail!', e); });
                    });
                    return false;
                });
            }
        },


        // ==========================================================
        //  --------------------- Create -------------------------
        // ==========================================================


        /**
         * Create user
         * @return function
         */
        create: function() {
            var self = this;
            // click functino
            document.querySelector('.create').addEventListener('click', function(e) {
                e.preventDefault();
                // render editor
                utils.render('#page', tmpl_editor, data_dashboard)
                    .then(function(response) {
                        // save function
                        self.save();
                        // back to dashboard function
                        self.back();
                        self.initEditor();
                    }, function(e) { console.log('Fail!', e); });
            });
        },


        // ==========================================================
        //  --------------------- Save -------------------------
        // ==========================================================


        /**
         * Save snippet
         * @return function
         */
        save: function() {
            var self = this;
            // click function
            document.querySelector('.save').addEventListener('click', function(e) {
                e.preventDefault();
                // data for firebase
                var forms = document.forms[0],
                    data = {
                        title: forms.title.value,
                        desc: utils.encode(forms.desc.value),
                        type: (forms.type.value) ? forms.type.value : 'Other',
                        code: utils.encode(editor.getValue()),
                    };
                // check empty fields
                if (forms.title.value === '') {
                    utils.notif('error', err_save.title, err_save.msg);
                } else {
                    // push data if good
                    utils.setDb('public', data)
                        .then(function(d) {
                            utils.notif('success', done_save.title, done_save.msg)
                                .then(function() {
                                    self.init();
                                    // errors
                                }, function(e) { console.log('Fail!', e); });
                        });
                }
            });
        },


        // ==========================================================
        //  --------------------- Update -------------------------
        // ==========================================================


        /**
         * Update snippet
         * @param  id
         * @return function
         */
        update: function(d) {
            var self = this;
            // click function
            document.querySelector('.save').addEventListener('click', function(e) {
                e.preventDefault();
                // data for firebase
                var forms = document.forms[0],
                    data = {
                        title: forms.title.value,
                        desc: utils.encode(forms.desc.value),
                        type: (forms.type.value) ? forms.type.value : 'Other',
                        code: utils.encode(editor.getValue()),
                    };
                // check empty fields
                if (forms.title.value === '') {
                    utils.notif('error', err_save.title, err_save.msg);
                } else {
                    // push data if good
                    utils.putDb('public', d, data)
                        .then(function(d) {
                            utils.notif('success', done_update.title, done_update.msg)
                                .then(function() {
                                    self.init();
                                }, function(e) { console.log('Fail!', e); });
                        });
                }
            });
        },


        // ==========================================================
        //  --------------------- Edit -------------------------
        // ==========================================================


        /**
         * Edit snippet
         * @param id
         * @return function
         */
        edit: function(d) {
            var self = this;
            // render editor
            utils.render('#page', tmpl_editor, data_dashboard)
                // init events and load editor
                .then(function(response) {
                    // save function
                    self.update(d);
                    // back to dashboard function
                    self.back();
                    // init codemirror
                    self.initEditor();
                }, function(e) { console.log('Fail!', e); })
                .catch(function(error) { console.log(error); })
                // get data of firebase
                .then(function(response) {
                    utils.getDb('public/' + d)
                        .then(function(response) {
                            var forms = document.forms[0];
                            if (forms) {
                                forms.title.value = response.title;
                                forms.desc.value = utils.decode(response.desc);
                                forms.type.value = response.type;
                                editor.setValue(utils.decode(response.code));
                            }
                        }, function(e) { console.log('Fail!', e); })
                        .catch(function(error) { console.log(error); });
                }, function(e) { console.log('Fail!', e); });
        },


        // ==========================================================
        //  --------------------- Delete -------------------------
        // ==========================================================


        /**
         * Delete snippet
         * @param  id
         * @return function
         */
        delete: function(d) {
            var self = this;
            utils.removeDb('/public/' + d)
                .then(function(response) {
                    utils.notif('success', done_remove.title, done_remove.msg, null);
                }, function(e) { console.log('Fail!', e); })
                .then(function() {
                    self.init();
                }, function(e) { console.log('Fail!', e); });
        },


        // ==========================================================
        //  --------------------- Editor -------------------------
        // ==========================================================


        /**
         * Init editor codemirror
         * @return function
         */
        initEditor: function() {
            var self = this;
            // init codemirror
            if (document.querySelector('#editor')) {
                var pending;
                editor = CodeMirror.fromTextArea(document.querySelector('#editor'), {
                    mode: "scheme",
                    lineNumbers: true,
                    onChange: function() {
                        clearTimeout(pending);
                        setTimeout(self.update, 400);
                    }
                });
                editor.setOption("mode", self.looksLikeScheme(editor.getValue()) ? "scheme" : "javascript");
                editor.setOption('theme', 'material');
            }
        },
        /**
         *  Check theme
         * @param  string
         * @return function
         */
        looksLikeScheme: function(code) {
            return !/^\s*\(\s*function\b/.test(code) && /^\s*[;\(]/.test(code);
        },



        // ==========================================================
        //  --------------------- Back -------------------------
        // ==========================================================


        /**
         * Back button
         * @return function
         */
        back: function() {
            var self = this;
            // on click
            document.querySelector('#back').addEventListener('click', function(e) {
                e.preventDefault();
                return self.init();
            });
        }
    };
})();


window.addEventListener('load', function() {
    utils.notif('', 'Loading', 'Please Wait a momment')
        .then(function() {
            return app.init();
        }, function(e) { console.log('Fail!', e); });
});
