var utils = (function() {
    'use strict';
    return {
        ref: new Firebase(firebaseUrl),
        /**
         * Micro - Templating
         * Example:
         *   var data = {title:'Hello World'}
         *   var template = '<h1>{{title}}</h1>'
         *   var render = utils.tmpl(template);
         *   document.body.innerHTML = render(data);
         */
        tmpl: function(t) {
            /*jslint evil: true */
            var code = 'with(o){return \'' + t.replace(/{{\s*(.+?)\s*}}/g, '\'+($1)+\'') + '\';}';
            return new Function('o', code);
        },
        // filter links
        FilterLinks: function(element, selector) {
            var self = this,
                div = document.querySelector(element);
            if(div) div.addEventListener('change', function(e) {
                // if all remove all elements
                if (div.value === 'all') {
                    // first show all view class
                    self.Each(selector, function(e) {
                        e.classList.remove('view');
                    });
                    // no show init animation
                    self.Animate(selector);
                } else {
                    // if not click all remove all elements
                    self.Each(selector, function(e) {
                        e.classList.remove('view');
                    });
                }
                // show animation for current elements
                self.Animate('.' + div.value);
            });
        },
        FilterText: function(input, selector) {
            var self = this,
                filter = document.querySelector(input);
            if(filter) filter.addEventListener('keyup', function(e) {
                self.Each(selector, function(elem) {
                    var text = elem.textContent.toLowerCase(),
                    val = filter.value.toLowerCase();
                    elem.parentNode.style.display = text.indexOf(val) === -1 ? 'none' : 'block';
                });
            });
        },
        // each array
        Each: function(el, callback) {
            var allDivs = document.querySelectorAll(el),
                alltoArr = Array.prototype.slice.call(allDivs);
            Array.prototype.forEach.call(alltoArr, function(selector, index) {
                if (callback) return callback(selector);
            });
        },
        // Animate single items
        Animate: function(item) {
            return (function show(counter) {
                setTimeout(function() {
                    var element = document.querySelectorAll(item)[counter];
                    if (typeof element != 'undefined') element.classList.add('view');
                    counter++;
                    if (counter < document.querySelectorAll(item).length) show(counter);
                }, 50);
            })(0);
        },
        /**
         *  Escape html tagsto &lt;
         * Example:
         *   utils.escapeHtml('<h1>Hello world</h1>');
         */
        escapeHtml: function(str) {
            var entityMap = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': '&quot;',
                "'": '&#39;',
                "/": '&#x2F;'
            };
            return String(str).replace(/[&<>"'\/]/g, function(s) {
                return entityMap[s];
            });
        },
        /**
         * Render templates
         * Example:
         *  utils.render('#page', tmpl_login, data_login);
         */
        render: function(selector, tmpl, data) {
            return new Promise(function(resolve,reject){
                // Compile your template
                var render = utils.tmpl(tmpl);
                document.querySelector(selector).innerHTML = render(data);
                if(render(data)){
                    resolve(data);
                }else{
                    reject(Error('Error'));
                }
            });
        },
        /**
         * Simple notification
         * Example:
         *   utils.notif('success','Atention !','lorem ipsum');
         */
        notif: function(type, header, text) {
            return new Promise(function(resolve,reject){
                var div = document.createElement('div');
                div.className = 'notification ' + type;
                div.innerHTML = ['<div class="info"><h3>',header,'</h3><p>',text,'</p></div>'].join('');
                document.body.appendChild(div);
                var wait = setTimeout(function() {
                    if(document.body.removeChild(div)){
                        resolve();
                    }else{
                        reject(Error('Error'));
                    }
                    clearTimeout(wait);
                },1000);
            }).catch(function(e){
                console.log(e);
            });
        },
        /**
         * Login function
         * Example:
         *   utils.login({email:'',password:''},callback);
         */
        login: function(data, callback) {
            var self = this;
            this.ref.authWithPassword(data, function(e, d) {
                    if (e) {
                        if (callback) return callback(e);
                    } else {
                        storage.setItem('user_uid', d.uid);
                        storage.setItem('user_email', d.password.email);
                        storage.setItem('user_thumb', d.password.profileImageURL);
                        if (callback) return callback(d);
                    }
                },
                // destroy session on close browser
                { remember: "sessionOnly" });
        },
        /**
         * Logout
         * Example:
         *   utils.logout();
         */
        logout: function() {
            if (this.ref.getAuth()) {
                storage.removeItem('user_uid');
                storage.removeItem('user_email');
                storage.removeItem('user_thumb');
                this.ref.unauth();
                window.location.reload();
            } else {
                return false;
            }
        },
        /**
         * Check login
         * Example:
         *   utils.isLogin() = true or false
         */
        isLogin: function() {
            if (this.ref.getAuth()) return true;
            else return false;
        },
        /**
         * Db get
         * Example:
         *   utils.getDb('public');
         */
        getDb: function(type, callback) {
            var self = this;
            return new Promise(function(resolve,reject){
                if (self.ref.getAuth()) {
                    fbase.child(type).on('value', function(d) {
                        if(d){
                            resolve(d.val());
                        }else{
                            reject(Error('Error'));
                        }
                    });
                } else {
                    utils.notif('error', err_login.title, err_login.msg);
                }
            });
        },
        /**
         * Db set
         * Example:
         *   utils.setDb('public',data,callback);
         */
        setDb: function(type, data) {
            var self = this;
            return new Promise(function(resolve,reject){
                if (self.ref.getAuth()) {
                    return fbase.child(type).push(data, resolve).catch(reject);
                } else {
                    utils.notif('error', err_login.title, err_login.msg);
                }
            });
        },
        /**
         * Db put
         * Example:
         *   utils.putDb('public',id,data,callback);
         */
        putDb: function(type, id, data) {
            var self = this;
            return new Promise(function(resolve,reject){
                if (self.ref.getAuth()) {
                    return fbase.child(type).child(id).set(data, resolve).catch(reject);
                } else {
                    utils.notif('error', err_login.title, err_login.msg);
                }
            });
        },
        /**
         * Db delete
         * Example:
         *   utils.putDb(id,callback);
         */
        removeDb: function(id) {
            var self = this;
            return new Promise(function(resolve,reject){
                if (self.ref.getAuth()) {
                    if (fbase.child(id).remove()) {
                        resolve('success');
                    }else{
                        reject('error');
                    }
                } else {
                    utils.notif('error', err_login.title, err_login.msg);
                }
            });
        },
        /**
         * Encode data
         * Example:
         *   utils.encode('hello world');
         */
        encode: function(str) {
            return window.btoa(unescape(encodeURIComponent(str)));
        },
        /**
         * Decode data
         * Example:
         *   utils.decode('aGVsbG8gd29ybGQ=');
         */
        decode: function(str) {
            return decodeURIComponent(escape(window.atob(str)));
        }
    };
})();
