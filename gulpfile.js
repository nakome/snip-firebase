// require files
var jshint = require('gulp-jshint'),
    prettify = require('gulp-jsbeautifier'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss'),
    gulp = require('gulp'),
    browserSync = require('browser-sync').create();


/**
*   --------------------
*          Paths
*   --------------------
*/

// Paths
var paths = {
    'html': './src/',
    'assets': './src/assets/',
    'js': './src/assets/js/',
    'css': './src/assets/css/',
    'vendor': './src/assets/vendor/',
    'images': './src/assets/img/',
    // output dir
    'dist': './dist/'
};


/**
*   --------------------
*          Tasks
*   --------------------
*/


// clone index and vendor folder
gulp.task('clone', function() {
    //gulp.src(paths.vendor+'**/*').pipe(gulp.dest(paths.dist + 'assets/vendor/'));
    gulp.src(paths.images+'**/*').pipe(gulp.dest(paths.dist + 'assets/img/'));
});

// html files
gulp.task('html', function() {
    gulp.src([paths.html + '*.html'])
        // pretty html
        .pipe(prettify({
            indentSize: 2
        }))
        .pipe(gulp.dest(paths.dist))
        .pipe(browserSync.stream());
});


// javascript files
gulp.task('javascript', function() {
    return gulp.src([
            './src/assets/vendor/codemirror/codemirror.js',
            './src/assets/vendor/codemirror/codemirror-modes.js',
            './src/assets/vendor/FileSaver/FileSaver.min.js',
            './src/assets/vendor/highlight/highlight.pack.js',
            './src/assets/js/data.js',
            './src/assets/js/utils.js',
            './src/assets/js/templates.js',
            './src/assets/js/app.js',
        ])
        // compress
        .pipe(uglify())
        // concat
        .pipe(concat('bundle.js'))
        // move to
        .pipe(gulp.dest(paths.dist + 'assets/js'))
        .pipe(browserSync.stream());
});


gulp.task('css', function () {
  gulp.src([
        './src/assets/vendor/normalize/normalize.css',
        './src/assets/vendor/codemirror/codemirror.css',
        './src/assets/vendor/codemirror/material.css',
        './src/assets/vendor/highlight/arduino-light.css',
        './src/assets/css/app.css',
    ])
    .pipe(concatCss("bundle.css"))
    .pipe(uglifycss({"maxLineLen": 80,"uglyComments": true}))
    .pipe(gulp.dest(paths.dist + 'assets/css'))
    .pipe(browserSync.stream());
});

// Static server
gulp.task('server', ['javascript','css','clone','html'], function() {
    browserSync.init({
        server: {
            baseDir: paths.dist
        }
    });
    // watch changes on this tasks
    gulp.watch(paths.css + '*.css', ['css']);
    gulp.watch(paths.js + '*.js', ['javascript']);
    gulp.watch(paths.html+'*.html').on('change', browserSync.reload);
    gulp.watch(paths.css+'*.css').on('change', browserSync.reload);
});



/**
*   --------------------
*      Group tasks
*   --------------------
*/
// Check with jshint
gulp.task('jshint', function() {
    return gulp.src([
            './src/assets/js/data.js',
            './src/assets/js/utils.js',
            './src/assets/js/templates.js',
            './src/assets/js/app.js',
        ])
        // check with jshit
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
// default task
gulp.task('default', ['server']);
// build only render files
gulp.task('build', ['javascript','css','clone','html']);
